### Overview
This will deploy the Datadog log forwarder lambda to an AWS account.  This lambda is created and maintained by Datadog.  The cloudformation template to deploy the Datadog log forwarder and details on its usage can be found at https://github.com/DataDog/datadog-serverless-functions/tree/aws-dd-forwarder-3.13.0/aws/logs_monitoring 
