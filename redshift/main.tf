terraform {
  backend "s3" {
    key    = "terraform/reporting-database/statefile"
    region = "us-east-1"
  }
}

provider "aws" {
  region = var.aws_region
}

data "aws_caller_identity" "current" {}

######
# VPC Info
######
data "aws_subnet" "redshift_subnet0" {
  filter { 
    name   = "tag:Name"
    values = ["cloud-network-vpc/VPC/PrivateSubnet1"]
  }
}

data "aws_subnet" "redshift_subnet1" {
  filter { 
    name   = "tag:Name"
    values = ["cloud-network-vpc/VPC/PrivateSubnet2"]
  }
}

data "aws_subnet" "redshift_subnet2" {
  filter { 
    name   = "tag:Name"
    values = ["cloud-network-vpc/VPC/PrivateSubnet3"]
  }
}

data "aws_vpc" "selected" {
  filter { 
    name   = "tag:Name"
    values = ["cloud-network-vpc/VPC"]
  }
}

#TODO research method to do cross account lookup
#data "aws_kms_alias" "reporting_db_test" {
#  name = "alias/reporting_db_${var.environment_name}"
#}

data "template_file" "role_policy" {
    template = file("${path.module}/policies/role_policy.json")
}

data "template_file" "iam_policy_s3_logging" {
    template = file("${path.module}/policies/iam_policy_s3_logging.json")
    vars = {
      bucket_arn = module.s3_bucket.this_s3_bucket_arn
    }
}

data "template_file" "iam_policy_kms" {
    template = file("${path.module}/policies/iam_policy_kms.json")
    vars = {
      kms_key_arn = var.aws_kms_key_arn #aws_kms_alias.reporting_db.target_key_arn
      key_account_id = var.key_account_id
    }
}

data "template_file" "bucket_policy" {
    template = file("${path.module}/policies/bucket_policy.json")
    vars = {
      bucket_arn = module.s3_bucket.this_s3_bucket_arn
    }
}

data "template_file" "iam_policy_db_user_limited" {
    template = file("${path.module}/policies/iam_db_user.json")
    vars = {
      region       = var.aws_region
      account_id   = data.aws_caller_identity.current.account_id
      cluster_name = module.redshift.this_redshift_cluster_id
      dbuser_name  = aws_iam_user.rs_jupyter.name
      dbgroup_name = var.rs_group_notebook_schema_write
    }
}

data "template_file" "iam_policy_db_user_ro" {
    template = file("${path.module}/policies/iam_db_user.json")
    vars = {
      region       = var.aws_region
      account_id   = data.aws_caller_identity.current.account_id
      cluster_name = module.redshift.this_redshift_cluster_id
      dbuser_name  = aws_iam_user.rs_tableau.name
      dbgroup_name = var.rs_group_all_schema_read
    }
}

data "template_file" "users_groups_sql" {
  template = file("${path.module}/sql/users_groups_template.sql")
  vars = {
    rs_group_all_schema_read      = var.rs_group_all_schema_read
    rs_group_notebook_schema_write = var.rs_group_notebook_schema_write
  }
}
########################
# IAM roles and policies
########################
resource "aws_iam_role" "this" {
  name               = "reporting_db_role"
  assume_role_policy = data.template_file.role_policy.rendered
}

resource "aws_iam_policy" "s3_logging" {
  name        = "reporting_db_policy_s3_logging"
  path        = "/"
  description = "Policy for reporting db cluster"
  policy      = data.template_file.iam_policy_s3_logging.rendered
}

resource "aws_iam_policy" "kms" {
  name        = "reporting_db_policy_kms"
  path        = "/"
  description = "Policy for reporting db cluster"
  policy      = data.template_file.iam_policy_kms.rendered
}

resource "aws_iam_role_policy_attachment" "reporting-attach" {
  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.s3_logging.arn
}

resource "aws_iam_role_policy_attachment" "kms-attach" {
  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.kms.arn
}

##########################
# IAM Database Azure Roles
##########################
resource "aws_iam_role" "rs_azuresso_ro" {
  name               = "rs_azuresso_ro"
  assume_role_policy = data.template_file.role_policy.rendered
}

resource "aws_iam_policy" "rs_azuresso_ro" {
  name        = "reporting_db_policy_azuresso_ro"
  path        = "/"
  description = "Policy for reporting db cluster"
  policy      = data.template_file.iam_policy_db_user_ro.rendered
}

resource "aws_iam_role_policy_attachment" "rs_azuresso_ro_attach" {
  role       = aws_iam_role.rs_azuresso_ro.name
  policy_arn = aws_iam_policy.rs_azuresso_ro.arn
}

resource "aws_iam_role" "rs_azuresso_notebook" {
  name               = "rs_azuresso_notebook"
  assume_role_policy = data.template_file.role_policy.rendered
}

resource "aws_iam_policy" "rs_azuresso_notebook" {
  name        = "reporting_db_policy_azuresso_notebook"
  path        = "/"
  description = "Policy for reporting db cluster"
  policy      = data.template_file.iam_policy_db_user_limited.rendered
}

resource "aws_iam_role_policy_attachment" "rs_azuresso_notebook_attach" {
  role       = aws_iam_role.rs_azuresso_notebook.name
  policy_arn = aws_iam_policy.rs_azuresso_notebook.arn
}

#resource "aws_iam_role" "rs_azuresso_db_admin" {
#  name               = "rs_azuresso_db_admin"
#  assume_role_policy = data.template_file.role_azure_policy.rendered
#}

#resource "aws_iam_policy" "rs_azuresso_db_admin" {
# name        = "reporting_db_policy_azuresso_db_admin"
#  path        = "/"
#  description = "Policy for reporting db cluster"
#  policy      = data.template_file.iam_policy_db_user_admin.rendered
#}

#resource "aws_iam_role_policy_attachment" "rs_azuresso_attach_db_admin" {
#  role       = aws_iam_role.rs_azuresso_db_admin.name
#  policy_arn = aws_iam_policy.rs_azzuresso_db_admin.arn
#}

###############################
# IAM Database Service Accounts
###############################
### Jupyter Notebook
resource "aws_iam_user" "rs_jupyter" {
  name        = "rs_jupyter"
}

resource "aws_iam_policy" "rs_db_cred_limited" {
  name        = "reporting_db_policy_cred_limited"
  path        = "/"
  description = "Policy for reporting db cluster to allow temporary credentials to read facts/dimensions and write to analytics schema"
  policy      = data.template_file.iam_policy_db_user_limited.rendered
}

resource "aws_iam_user_policy_attachment" "rs_db_cred_limited_attach" {
  user       = aws_iam_user.rs_jupyter.name
  policy_arn = aws_iam_policy.rs_db_cred_limited.arn
}

### Tableau
resource "aws_iam_user" "rs_tableau" {
  name        = "rs_tableau"
}

resource "aws_iam_policy" "rs_db_cred_ro" {
  name        = "reporting_db_policy_cred_ro"
  path        = "/"
  description = "Policy for reporting db cluster to allow temporary credentials to read facts/dimensions and write to analytics schema"
  policy      = data.template_file.iam_policy_db_user_ro.rendered
}

resource "aws_iam_user_policy_attachment" "rs_db_cred_ro_attach" {
  user       = aws_iam_user.rs_tableau.name
  policy_arn = aws_iam_policy.rs_db_cred_ro.arn
}

######################################
# Secret for access key and secret key
######################################
#TODO add an encryption key in kms for improved security
### Jupyter Notebook
resource "aws_iam_access_key" "rs_jupyter_key" {
  user = aws_iam_user.rs_jupyter.name
}

resource "aws_secretsmanager_secret" "rs_jupyter_secret" {
  name = "ad-hoc-reporting/rs_jupyter_user_key"
  description = "The AWS secret key and access key to use the account"
}

resource "aws_secretsmanager_secret_version" "rs_jupyter_secret_value" {
  secret_id = aws_secretsmanager_secret.rs_jupyter_secret.id
  secret_string = "{ \"access_key\": \"${aws_iam_access_key.rs_jupyter_key.id}\", \"secret_key\": \"${aws_iam_access_key.rs_jupyter_key.secret}\"}"
}

### Tableau
#TODO add an encryption key in kms for improved security
resource "aws_iam_access_key" "rs_tableau_key" {
  user = aws_iam_user.rs_tableau.name
}

resource "aws_secretsmanager_secret" "rs_tableau_secret" {
  name = "ad-hoc-reporting/rs_tableau_user_key"
  description = "The AWS secret key and access key to use the account"
}

resource "aws_secretsmanager_secret_version" "rs_tableau_secret_value" {
  secret_id = aws_secretsmanager_secret.rs_tableau_secret.id
  secret_string = "{ \"access_key\": \"${aws_iam_access_key.rs_tableau_key.id}\", \"secret_key\": \"${aws_iam_access_key.rs_tableau_key.secret}\"}"
}

#####################
# S3 Log Bucket
#####################
module "s3_bucket" {
  source                  = "terraform-aws-modules/s3-bucket/aws"
  bucket                  = "logs-reporting-db-${data.aws_caller_identity.current.account_id}"
  acl                     = "private"
  restrict_public_buckets = true
  force_destroy           = false
  attach_policy           = true
  policy                  = data.template_file.bucket_policy.rendered
}

###########################
# Security group
###########################
module "sg" {
  source  = "terraform-aws-modules/security-group/aws//modules/redshift"
  version = "~> 3.0"
  name    = "redshift"
  vpc_id  = data.aws_vpc.selected.id

  # Allow ingress rules to be accessed only within current VPC and LOB network
  ingress_cidr_blocks = [data.aws_vpc.selected.cidr_block, "10.200.200.0/21", "10.223.0.0/16", "10.5.0.0/18"]
  
  # Allow all rules for all protocols
  egress_rules = ["all-all"]
}

resource "aws_ssm_parameter" "redshift_sg_param" {
  name        = "/AdHocReporting/RedshiftSecurityGroupId"
  description = "The security group ID used by the redshift cluster. It is shared out so that Glue Jobs may utilitize it for connections to the cluster."
  type        = "String"
  value       = module.sg.this_security_group_id
}

###########
# Redshift
###########
module "redshift" {
  source = "./modules/lob_redshift"
  #prevent_destroy = true
  depends_on = [
    aws_iam_role_policy_attachment.reporting-attach,
    module.s3_bucket,
    module.sg
  ]

  #CLUSTER CONFIG
  cluster_identifier                  = "lob-reporting"
  cluster_node_type                   = "dc2.large"
  cluster_number_of_nodes             = 2
  require_ssl                         = "true"
  automated_snapshot_retention_period = 31 #Days
  final_snapshot_identifier           = "0" #Bug in the module

  #LOGGING CONFIG
  cluster_iam_roles            = [aws_iam_role.this.arn]
  enable_logging               = true
  enable_user_activity_logging = "true"
  logging_bucket_name          = module.s3_bucket.this_s3_bucket_id

  #DB CONFIG
  cluster_database_name   = "reporting"
  cluster_master_username = var.rs_master_username
  cluster_master_password = var.rs_master_pass
  encrypted               = true
  kms_key_id              = var.aws_kms_key_arn #aws_kms_alias.reporting_db.target_key_arn

  #NETWORK
  publicly_accessible     = false
  vpc_security_group_ids  = [module.sg.this_security_group_id]
  subnets                 = [data.aws_subnet.redshift_subnet0.id, data.aws_subnet.redshift_subnet1.id, data.aws_subnet.redshift_subnet2.id]
}

data "aws_subnet" "redshift_load_balancer_subnet" {
  vpc_id            = data.aws_vpc.selected.id
  availability_zone = module.redshift.this_redshift_cluster_availability_zone
  filter {
    name = "tag:Name"
    values = ["*Private*"]
  }
}

resource "aws_ssm_parameter" "redshift_subnet_id_param" {
  name        = "/AdHocReporting/RedshiftSubnetId"
  description = "The subnet ID where the leader node of the redshift cluster lives"
  type        = "String"
  value       = data.aws_subnet.redshift_load_balancer_subnet.id
}

resource "aws_ssm_parameter" "redshift_az_param" {
  name        = "/AdHocReporting/RedshiftAZ"
  description = "The availability zone of the leader node of the redshift cluster"
  type        = "String"
  value       = module.redshift.this_redshift_cluster_availability_zone
}

resource "aws_ssm_parameter" "redshift_jdbc_url_param" {
  name        = "/AdHocReporting/JDBCURL"
  description = "The JDBC URL for the redshift cluster"
  type        = "String"
  #TODO Replace with the IAM temporary credentials model 
  #https://docs.aws.amazon.com/redshift/latest/mgmt/generating-iam-credentials-configure-jdbc-odbc.html
  value       = "jdbc:redshift://${module.redshift.this_redshift_cluster_endpoint}/${module.redshift.this_redshift_cluster_database_name}"
}

############################
# Redshift Group Permissions
############################
# TODO consider adapting this project to terraform 0.13 as this section isn't declarative
# https://github.com/frankfarrell/terraform-provider-redshift
resource "null_resource" "psql_install" {
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = "apk -v --update add postgresql-client"
  }
}

resource "local_file" "users_groups_sql" {
  content = data.template_file.users_groups_sql.rendered
  filename = "sql/users_groups.sql"
}

resource "null_resource" "db_users_groups" {
  depends_on = [
    null_resource.psql_install,
    local_file.users_groups_sql
  ]
  triggers = {
    always_run = timestamp()
  }
  
  # Made this a file execution as file execution doesn't stop on each query that has an error, no easy way to declaratively create users
  provisioner "local-exec" {
    command = "psql --file=sql/users_groups.sql" #EXAMPLE OF TEMPLATE AS COMMANDS -c \"${data.template_file.users_groups_sql.rendered}\""

    environment = {
      PGUSER     = var.rs_master_username
      PGPASSWORD = var.rs_master_pass
      PGHOST     = module.redshift.this_redshift_cluster_hostname
      PGPORT     = 5439
      PGDATABASE = "reporting"
    }
  }
# TODO: to make local-exec more declarative add a when = "destroy" command
#  provisioner "local-exec" {
#     when = "destroy"
#     command = "psql --file=sql/cleanup.sql",
#    }
}

#CREATE SCHEMAS
resource "null_resource" "db_schemas" {
  depends_on = [
    null_resource.psql_install
  ]

  for_each = toset(var.schema_list)
  provisioner "local-exec" {
    command = "psql -c 'create schema if not exists ${each.value};'"

    environment = {
      PGUSER     = var.rs_master_username
      PGPASSWORD = var.rs_master_pass
      PGHOST     = module.redshift.this_redshift_cluster_hostname
      PGPORT     = 5439
      PGDATABASE = "reporting"
    }
  }
}

#GRANT SCHEMAS
resource "null_resource" "db_grants_ro" {
  depends_on = [
    null_resource.psql_install,
    null_resource.db_schemas,
    null_resource.db_users_groups
  ]
  triggers = {
    always_run = timestamp()
  }
  for_each = toset(var.schema_list)
  provisioner "local-exec" {
    command = "sleep ${index(var.schema_list, each.value) * 10} && psql -c 'GRANT USAGE ON SCHEMA ${each.value} TO GROUP ${var.rs_group_all_schema_read};' && sleep 5 && psql -c 'GRANT select ON all tables IN schema ${each.value} TO GROUP ${var.rs_group_all_schema_read};'"

    environment = {
      PGUSER     = var.rs_master_username
      PGPASSWORD = var.rs_master_pass
      PGHOST     = module.redshift.this_redshift_cluster_hostname
      PGPORT     = 5439
      PGDATABASE = "reporting"
    }
  }
}

resource "null_resource" "db_grants_read_limited" {
  depends_on = [
    null_resource.psql_install,
    null_resource.db_schemas,
    null_resource.db_users_groups,
    null_resource.db_grants_ro
  ]
  triggers = {
    always_run = timestamp()
  }
  for_each = toset(var.schema_list)
  provisioner "local-exec" {
    command = "sleep ${index(var.schema_list, each.value) * 10} && psql -c 'GRANT USAGE ON SCHEMA ${each.value} TO GROUP ${var.rs_group_notebook_schema_write};' && sleep 5 && psql -c 'GRANT select ON all tables IN schema ${each.value} TO GROUP ${var.rs_group_notebook_schema_write};'"

    environment = {
      PGUSER     = var.rs_master_username
      PGPASSWORD = var.rs_master_pass
      PGHOST     = module.redshift.this_redshift_cluster_hostname
      PGPORT     = 5439
      PGDATABASE = "reporting"
    }
  }
}

resource "null_resource" "db_grants_limited" {
  depends_on = [
    null_resource.psql_install,
    null_resource.db_schemas,
    null_resource.db_users_groups,
    null_resource.db_grants_read_limited
  ]
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = "psql -c 'GRANT ALL ON schema notebooks TO GROUP ${var.rs_group_notebook_schema_write};'"

    environment = {
      PGUSER     = var.rs_master_username
      PGPASSWORD = var.rs_master_pass
      PGHOST     = module.redshift.this_redshift_cluster_hostname
      PGPORT     = 5439
      PGDATABASE = "reporting"
    }
  }
}

