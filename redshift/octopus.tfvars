aws_region = "#{AWS_REGION}"

rs_master_username = "#{REPORTING_DB_USER}"

rs_master_pass = "#{REPORTING_DB_PASSWORD}"

aws_kms_key_arn = "#{REPORTING_KMS_KEY_ARN}"
