variable "aws_region" {
  default = "us-east-1"
}

variable "rs_master_username" {
  default = "emmetb"
}

variable "rs_master_pass" {
  default = "MySecretPassw0rd"
}

variable "rs_group_all_schema_read" {
  default = "all_schema_read"
}

variable "rs_group_notebook_schema_write" {
  default = "notebook_schema_write"
}

variable "environment_name" {
  default = "dev"
}

variable "key_account_id" {
  default = "555555555555"
}

variable "aws_kms_key_arn" {
  default = "arn:aws:kms:us-east-1:555555555555:key/aaaaaaaa-5555-5555-ffff-5555aaaa0000"
}

variable "schema_list" {
  default = [
    "dimensions",
    "facts",
    "Information_schema",
    "public",
    "staging",
    "analytics",
    "notebooks"
  ]
}