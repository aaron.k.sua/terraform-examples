### AWS Installation:
cd to <repo_root>/zeppelin 
```terraform init && terraform apply -auto-approve```

from Dashlane copy the private key to $HOME/.ssh/zeppelin-key.pem

### Faster/Local:
build the docker image:
cd to <repo_root>/zeppelin/Docker
```docker build -t glue_zeppelin```

run a script to start the container:
```zeppelin```

### Slower/Shared:
after the terraform runs, navigate to the AWS Glue console to create the Zeppelin notebook server.  The AWS UI does operations that are unknown or not supported by cloud formation, so creating the notebook server must be done manually.

Browse to ETL > Dev Endpoints.  Select the ```zeppelin-endpoint``` , click Action > Create Zeppelin Notebook server.

Ensure it is on a public subnet (automatically selected) and has a public ip (this is a checkbox).  Select existing IAM role (AWSGlueServiceNotebookRoleZeppelin) and  EC2 key pair (zeppelin-key).  

The Zeppelin Notebook server will get created with the same security group as the Dev Endpoints (glue-dev-endpoint-sg).  After the instance has been created, change its security group to zeppelin-external-access-sg.

ssh to it:
```ssh ec2-user@<public_ipaddress>``` or ```ssh -i <path to zeppelin-key.pem> ec2-user@<public_ipaddress>``` if your ssh keystore is not in your path.
and run the script:
```python setup_notebook_server.py```

this will generate an sshkey and register it with the dev endpoint.
it can also generate a self signed cert

IF the endpoint ever changes address there are 3 files to edit with the new address.
$HOME/hadoopconf/yarn-site.xml 
$HOME/hadoopconf/core-site.xml 
etc/autossh.host

Additionally if the EC2 is ever restarted, zeppelin does not come back up automatically.
Simply rerun python setup_notebook_server.py

### Notes:
The above will create servers with self signed certs, Chrome tends to not allow this and so Safari or Firefox are recommended.

CloudFormation does not provide enough resource looks ups, in particular network and Glue Dev Endpoint details, and EC2 key pairs, as well as managing the security group is more difficult.
Terraform does not provide a native component to create Glue Dev Endpoints, and would require a custom docker image with python installed.

For the Glue Endpoint.  Changes, such as the associated role, will not show a failure. However the changes are not actually applied.  The endpoint must be destroyed and recreated.

### Relevant links:
glue versions: https://docs.aws.amazon.com/glue/latest/dg/add-job.html

glue and zeppelin compatibility: https://docs.aws.amazon.com/glue/latest/dg/dev-endpoint-tutorial-local-notebook.html

ec2 setup details: https://docs.aws.amazon.com/glue/latest/dg/dev-endpoint-tutorial-EC2-notebook.html