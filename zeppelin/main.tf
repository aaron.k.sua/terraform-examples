terraform {
  backend "s3" {
    key    = "terraform/zeppelin/statefile"
    region = "us-east-1"
  }
}

provider "aws" {
  region = var.aws_region
}

######
# VPC Info
######
data "aws_subnet" "redshift_subnet0" {
  filter { 
    name   = "tag:Name"
    values = ["cloud-network-vpc/VPC/PrivateSubnet1"]
  }
}

data "aws_vpc" "selected" {
  filter { 
    name   = "tag:Name"
    values = ["cloud-network-vpc/VPC"]
  }
}

############
# Other Data
############
data "aws_caller_identity" "current" {}

data "template_file" "role_policy_devendpoint" {
    template = file("${path.module}/policies/role_policy_devendpoint.json")
}

data "template_file" "role_policy_zeppelin" {
    template = file("${path.module}/policies/role_policy_zeppelin.json")
}

data "aws_iam_role" "glue_job" {
  name = "FinxactGlueJobRole"
}

########################
# IAM roles and policies
########################
resource "aws_iam_role" "zeppelin" {
  name               = "AWSGlueServiceNotebookRoleZeppelin"
  assume_role_policy = data.template_file.role_policy_zeppelin.rendered
}

resource "aws_iam_role_policy_attachment" "zeppelin_service" {
  role       = aws_iam_role.zeppelin.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSGlueServiceNotebookRole"
}
resource "aws_iam_role_policy_attachment" "zeppelin_s3" {
  role       = aws_iam_role.zeppelin.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_instance_profile" "zeppelin" {
  name = aws_iam_role.zeppelin.name
  role = aws_iam_role.zeppelin.name
}
#####################
# S3 Log Bucket
#####################
module "s3_bucket" {
  source                  = "terraform-aws-modules/s3-bucket/aws"
  bucket                  = "ad-hoc-reporting-zeppelin-${data.aws_caller_identity.current.account_id}"
  acl                     = "private"
  restrict_public_buckets = true
  force_destroy           = false

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        sse_algorithm   =   "AES256"
      }
    }
  }
}

###########################
# Security group
###########################
module "sg-zeppelin" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"
  name    = "zeppelin-external-access-sg"
  vpc_id  = data.aws_vpc.selected.id

  # Allow ingress rules to be accessed only within current VPC and LOB network
  #ingress_cidr_blocks = [data.aws_vpc.selected.cidr_block, "10.200.200.0/21", "10.223.0.0/16"]
  ingress_with_cidr_blocks = [
    {
      rule        = "https-443-tcp"
      cidr_blocks = "54.85.241.122/32,208.127.94.223/32"
    },
    {
      rule        = "ssh-tcp"
      cidr_blocks = "208.127.94.223/32"
    }
  ]
  ingress_with_self = [
    {
      rule = "all-all"
    }
  ]
  # Allow all rules for all protocols
  egress_rules = ["all-all"]
}

module "sg-glue-dev-endpoint" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"
  name    = "glue-dev-endpoint-sg"
  vpc_id  = data.aws_vpc.selected.id

  # Allow ingress rules to be accessed only within current VPC and LOB network
  #ingress_cidr_blocks = [data.aws_vpc.selected.cidr_block, "10.200.200.0/21", "10.223.0.0/16"]
  ingress_cidr_blocks = ["10.0.0.0/8"]
  ingress_rules = ["ssh-tcp"]

  ingress_with_self = [
    {
      rule = "all-all"
    }
  ]
  # Allow all rules for all protocols
  egress_rules = ["all-all"]
}
#############
# EC2 Keypair
#############
resource "aws_key_pair" "zeppelin" {
  key_name   = "zeppelin-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCIqh6lAqW8drzFVDMqZrpjwJr9ruCyMx9VbDPwab2L5zi1r4Fgijpzkn+m3qDJoWWddvivfy2ZoViVbYZSfqXbfNoH42/qo/ALHVFSJ9v74+MF6LuQ/h6jXEBEPSzmAO2IzE9x2gNKWVk9zuRqfvsv3UqnjWR5DsDQVNceIjYcRPmK2qntv+NRQyrn94XuFoWviPzpTXodiDq9T5sY76jTUxgt+2pwwpvFpeckOVqMqs3yDG8+/Ua4nkkIijdahc1Uawuu131ubEwFKQ2TW854fbO0IgooitrZGrijxgpRP6o2erilrUmgLcYesfkr6NLZiR2C3ztZEK1mMu0y/jON"
}

################
# CloudFormation
################
resource "aws_cloudformation_stack" "zeppelin-glue-endpoint" {
#  depends_on = [
#    aws_iam_role_policy_attachment.glue_jobs
#  ]

  name       = "zeppelin-glue-endpoint"
  parameters = {
    GlueDevEndpointRoleArn = data.aws_iam_role.glue_job.arn
    GlueDevEndpointName    = var.glue_dev_endpoint_name
    GlueVersion            = "1.0"
    PythonVersion          = "3"
    SecurityGroupId        = module.sg-glue-dev-endpoint.this_security_group_id
    SubnetId               = data.aws_subnet.redshift_subnet0.id
    PublicKey              = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDOddZcJtyZrafRbEi/y3/MpyRwkln83x3Pq2mbWkYU7jGnEuHkQFpvRjOykIL2GDvAN0HSDrr/UkLRfzMQVrFXdEZ/6OIc8kUd1ddAw4t5mTfQO2B3GRg2O+OUu0UttJ/HxEYZiFZy+aGsOhjlyCtK3mw6lPpMGmoXrFYSqE5Ns92DvJ7aIop0Gi8NPOoiEp9Rw9zJe71T0gQrl1G1Q+a4tKleetr0F+TYp9kt/tFXwwPTyxDAF5sMCqoNvWprRU2SrcO0i8j31rewWD93FonNeVZ+RV+3Gt1ugLKZVdzVLUoLimswDpSchy+2zlxu/T+7h22YGKAJYtLYSRo2tXkb ec2-user@ip-10-1-181-234"
  }

  template_body = file("${path.module}/templates/glue_endpoint.yml")
}

resource "aws_cloudformation_stack" "zeppelin-glue-endpoint-local" {
#  depends_on = [
#    aws_iam_role_policy_attachment.glue_jobs
#  ]

  name       = "zeppelin-glue-endpoint-local"
  parameters = {
    GlueDevEndpointRoleArn = data.aws_iam_role.glue_job.arn
    GlueDevEndpointName    = "${var.glue_dev_endpoint_name}-local"
    GlueVersion            = "1.0"
    PythonVersion          = "3"
    SecurityGroupId        = module.sg-glue-dev-endpoint.this_security_group_id
    SubnetId               = data.aws_subnet.redshift_subnet0.id
    PublicKey              = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCIqh6lAqW8drzFVDMqZrpjwJr9ruCyMx9VbDPwab2L5zi1r4Fgijpzkn+m3qDJoWWddvivfy2ZoViVbYZSfqXbfNoH42/qo/ALHVFSJ9v74+MF6LuQ/h6jXEBEPSzmAO2IzE9x2gNKWVk9zuRqfvsv3UqnjWR5DsDQVNceIjYcRPmK2qntv+NRQyrn94XuFoWviPzpTXodiDq9T5sY76jTUxgt+2pwwpvFpeckOVqMqs3yDG8+/Ua4nkkIijdahc1Uawuu131ubEwFKQ2TW854fbO0IgooitrZGrijxgpRP6o2erilrUmgLcYesfkr6NLZiR2C3ztZEK1mMu0y/jON user@email.com"
  }

  template_body = file("${path.module}/templates/glue_endpoint.yml")
}
