variable "aws_region" {
  default = "us-east-1"
}

variable "environment_name" {
  default = "dev"
}

variable "key_account_id" {
  default = "555555555555"
}

variable "glue_dev_endpoint_name" {
  default = "zeppelin-endpoint"
}